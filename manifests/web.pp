#
# $Id$
# vim6:ts=2:sw=2:et:syntax=ruby

class dwarfgarden::web (
  $basedir = '/var/www/html/dwarfgarden',
  $dbserver,
  $dbname,
  $dbuser,
  $dbpassword ) {
#  require pgsql pourrait être ajouté: à tester
#  la classe pgsql est ajoutée dans nodes.pp
#
#  if $monitored {
#    include dwarf-garden::zabbix
#  }
# TODO: create iris role if not exist
# conflict with irisdb class
#  pgsql::rolecreate {$user:
#    password => $iris_pass,
#  }

  file { $basedir:
    source  => "puppet:///modules/dwarfgarden/web",
    recurse => true,
    ignore  => ['.svn','*~'],
    owner   => $apache::params::wwwuser,
    group   => $apache::params::wwwgroup,
    mode    => 0644,
  } -> 
  file { "$basedir/conf":
    ensure  => directory,
    owner   => $apache::params::wwwuser,
    group   => $apache::params::wwwgroup,
  } ->
  file { "$basedir/conf/config.php":
    content => template('dwarfgarden/config.php.erb'),
    owner   => $apache::params::wwwuser,
    group   => $apache::params::wwwgroup,
  }
  file { "/var/log/ipc" : 
    ensure => directory,
    owner   => $apache::params::wwwuser,
    group   => $apache::params::wwwgroup,
  }
  user::dir { "/opt/ipc" : } ->
  file { "/opt/ipc/workspace" : 
    ensure => directory,
    owner   => $apache::params::wwwuser,
    group   => $apache::params::wwwgroup,
  }
  cron { "cleanup IPC workspace":
    user        => $apache::params::wwwuser,
    command     => 'rm -rf /opt/ipc/workspace/*',
    minute      => '0',
    hour        => '21',
    weekday     => '*',
    monthday    => '*',
    month       => '*'
  }
  file {
  "/var/www/cgi-bin/download-release-packages.sh":
    source  => "puppet:///modules/dwarfgarden/scripts/download-release-packages.sh",
    owner   => $apache::params::wwwuser,
    group   => $apache::params::wwwgroup,
    mode    => '755';
  "/var/www/cgi-bin/check-properties-vs-packages-web.sh":
    source  => "puppet:///modules/dwarfgarden/scripts/check-properties-vs-packages-web.sh",
    owner   => $apache::params::wwwuser,
    group   => $apache::params::wwwgroup,
    mode    => '755';
  "/var/www/cgi-bin/checkpropsFile.sh":
    source  => "puppet:///modules/dwarfgarden/scripts/checkpropsFile.sh",
    owner   => $apache::params::wwwuser,
    group   => $apache::params::wwwgroup,
    mode    => '755';
  "/var/www/cgi-bin/shellotemplatator":
    source  => "puppet:///modules/dwarfgarden/scripts/shellotemplatator",
    owner   => $apache::params::wwwuser,
    group   => $apache::params::wwwgroup,
    mode    => '644';
  }
}
