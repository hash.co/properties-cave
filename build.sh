#!/usr/bin/env bash

CurrentDir="$(realpath `dirname $0`)"

_installPrerequisistes() {
    cat <<EOF
        You will need to install npm, docker and docker-compose for this build to work

        See https://nodejs.org/en/download/package-manager/#debian-and-ubuntu-based-linux-distributions
        See https://docs.docker.com/engine/installation/#server

        On Ubuntu:

sudo apt-get remove -y docker docker-engine docker.io
sudo apt-get update
sudo apt-get install -y apt-transport-https ca-certificates curl software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository -y "deb [arch=$(dpkg --print-architecture)] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
sudo apt-get update
sudo apt-get -y install docker-ce
curl -sL https://deb.nodesource.com/setup_6.x | sudo -E bash -
EOF
}

_checkDeps() {
    local hasdocker=$(which docker > /dev/null; echo "$?")
    local hasdockercompose=$(which docker-compose > /dev/null; echo "$?")
    local hasnpm=$(which npm > /dev/null; echo "$?")

    if [ $hasdocker -ne 0 -o $hasdockercompose -ne 0 -o $hasnpm -ne 0 ]
    then
        _installPrerequisistes
        exit 1
    fi
}

_build() {
    echo "installing developpement npm dependencies" 
    npm install || exit 1
    $CurrentDir/node_modules/.bin/gulp $@
}


_checkDeps 

_build "$@"

