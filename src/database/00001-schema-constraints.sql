
create or replace function check_properties_duplication() returns trigger
as $$
-- Returns an exception if the properties is already set. This means
-- the tuple (name,platform,environment,country, version) already exists.
DECLARE
    dupfound boolean;
BEGIN
    SELECT exists(
        select * from properties 
        where name = NEW.name 
        and NEW.id != id
        and ((platform = NEW.platform) or (platform is null and NEW.platform is null))
        and ((environment = NEW.environment) or (environment is null and NEW.environment is null)) 
        and ((version = NEW.version) or (version is null and NEW.version is null))
        and ((country = NEW.country) or (country is null and NEW.country is null))
    ) into dupfound;
    IF dupfound THEN
        RAISE EXCEPTION '% is being duplicated (name=%,platform=%,environment=%,country=%, version=%)' , NEW.name, NEW.name, NEW.platform, NEW.environment, NEW.country, NEW.version;
    ELSE
        RETURN NEW;
    END IF;
END;    
$$ language plpgsql ;

create or replace function check_properties_consistancy() returns trigger
as $$
BEGIN
    -- We should not have 2 non null values 
    IF (select count(*) from (VALUES(NEW.platform),(NEW.environment),(NEW.country)) t(v) where v is not null) > 1 THEN
        RAISE EXCEPTION '% should not define both platform (%), environment (%) AND country(%) ' , NEW.name,NEW.platform,NEW.environment,NEW.country ;
    ELSE
        RETURN NEW;
    END IF;
END;
$$ language plpgsql ;


create or replace function check_versions_consistancy() returns trigger
as $$
BEGIN
    IF NEW.version !~ E'^[\\d\.]+$' THEN
        RAISE EXCEPTION E'version % doesn''t match ''^[\\d\.]+$'' ',NEW.version;
    ELSIF NEW.version_limit !~ E'^[\\d\.]+$' THEN
        RAISE EXCEPTION E'version_limit % doesn''t match ''^[\\d\.]+$'' ',NEW.version_limit;
    END IF;
    IF NEW.version = NEW.version_limit THEN
        RAISE EXCEPTION E'version and version_limit can''t have the same value %',NEW.version_limit;
    END IF; 
    IF NEW.version is NULL THEN
        -- if NEW.Version_limit is not null. We need to be sure there is no overlap.
        -- we stop if there is a prop with: 
        --   * version < NEW.version_limit 
        IF exists( select * from properties
                where name = NEW.name
                and NEW.id != id
                and ((platform = NEW.platform) or (platform is null and NEW.platform is null))
                and ((environment = NEW.environment) or (environment is null and NEW.environment is null))
                and ((country = NEW.country) or (country is null and NEW.country is null))
                and ( NEW.version_limit is NULL 
                     OR ( version_between(NEW.version_limit, version, version_limit) AND NEW.version_limit != version ) 
                     OR NEW.version_limit = version_limit
                )) THEN
            RAISE EXCEPTION '% is creating a duplication for version_limit % ', NEW.name, NEW.version_limit;
        ELSE
            RETURN NEW;
        END IF;
    ELSE
        -- version is not null. 
        -- We reject if there is a property like:
        --  * version <= New.version < version_limit
        --  * or ( ( version <= New.version_limit < version_limit AND New.version_limit != version ) OR New.version_limit = version_limit ) (with New.version_limit not null)
        --  * or New.version <= version < New.version_limit
        IF exists(select * from properties
                where name = NEW.name
                and NEW.id != id
                and ((platform = NEW.platform) or (platform is null and NEW.platform is null))
                and ((environment = NEW.environment) or (environment is null and NEW.environment is null))
                and ((country = NEW.country) or (country is null and NEW.country is null))
                and ( version_between(NEW.version, version, version_limit)
                    OR (NEW.version_limit is not null 
                        and (( version_between(NEW.version_limit, version, version_limit) AND NEW.version_limit != version) OR New.version_limit = version_limit ))
                    OR (version is not null and version_between(version, NEW.version, NEW.version_limit))
                )) THEN
            RAISE EXCEPTION '% is creating a duplication for version % and version_limit % ', NEW.name, NEW.version, NEW.version_limit ;
        ELSE
            RETURN NEW;
        END IF;
    END IF;
END;
$$ language plpgsql;


CREATE CONSTRAINT TRIGGER verify_values_duplication_for_versions_on_update
AFTER UPDATE ON properties
FOR EACH ROW
WHEN (OLD.version IS DISTINCT FROM NEW.version OR OLD.version_limit IS DISTINCT FROM NEW.version_limit)
EXECUTE PROCEDURE check_versions_consistancy();

CREATE CONSTRAINT TRIGGER verify_values_duplication_for_versions
AFTER INSERT ON properties
FOR EACH ROW
EXECUTE PROCEDURE check_versions_consistancy();

CREATE CONSTRAINT TRIGGER verify_values_duplication
AFTER INSERT ON properties
FOR EACH ROW
EXECUTE PROCEDURE check_properties_duplication();

CREATE CONSTRAINT TRIGGER verify_values_duplication_on_update
AFTER UPDATE ON properties
FOR EACH ROW
WHEN (OLD.version IS DISTINCT FROM NEW.version 
	OR OLD.name IS DISTINCT FROM NEW.name
	OR OLD.environment IS DISTINCT FROM NEW.environment
	OR OLD.platform IS DISTINCT FROM NEW.platform
    OR OLD.country IS DISTINCT FROM NEW.country
)
EXECUTE PROCEDURE check_properties_duplication();

CREATE CONSTRAINT TRIGGER verify_only_environment_or_platform_set
AFTER INSERT OR UPDATE ON properties
FOR EACH ROW
EXECUTE PROCEDURE check_properties_consistancy();

ALTER TABLE properties ADD CONSTRAINT no_space_in_property_name check (name !~ '[[:space:]]');

