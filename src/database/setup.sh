#!/bin/bash

SRCDIR="$(dirname $0)"
LOGFILE=/tmp/setupdb.log


DBUSER=${DBUSER:-"postgres"}
DBSERVER=${DBSERVER:-"postgresql-db"} 
DBPORT=${DBPORT:-"5432"} 
DBNAME=${DBNAME:-"propertiescave"} 

# Try to connect to the server for 30sec
for i in $(seq 1 6)
do
    psql -At -U $DBUSER -h $DBSERVER -p $DBPORT -d template1 -c 'select now()' &> /dev/null ; r=$?
    if [ $r -eq 2 ] 
    then
        sleep 5
    else
        break
    fi
done


psql -At -U $DBUSER -h $DBSERVER -p $DBPORT -d template1 -c 'select datname from pg_catalog.pg_database' | grep -q "$DBNAME" ; r=$?

if [ $r -ne 0 ]
then
    echo -n -e "The database '$DBNAME' doesn't exist. Trying to create it ... \t"
    psql -At -U $DBUSER -h $DBSERVER -p $DBPORT -d template1 -c 'create database "'$DBNAME'" owner "'$DBUSER'"' ; r=$?
    [ $r -ne 0 ] && exit 1
fi

psql -At -U $DBUSER -h $DBSERVER -p $DBPORT -d $DBNAME \
    -c "select tablename from pg_catalog.pg_tables where schemaname = 'public';" | grep -q "played_scripts" ; r=$?

if [ $r -ne 0 ]
then
    echo -n -e "Creating 'played_scripts' table for schema changes history ... \t"
    psql -At -U $DBUSER -h $DBSERVER -p $DBPORT -d $DBNAME \
        -c "create table played_scripts (name text primary key, date timestamp default now())";  r=$?
    [ $r -ne 0 ] && exit 1
fi

\ls "$SRCDIR"/*.sql | sort | while read sqlfile
do
    psql -At -U $DBUSER -h $DBSERVER -p $DBPORT -d $DBNAME \
        -c "select name from played_scripts where name = '$(basename $sqlfile)';" | grep -q "$(basename $sqlfile)" ; r=$?

    [ $r -eq 0 ] && echo -e "Skipping \t [$sqlfile]" && continue

    echo -e -n "Playing \t [$sqlfile] \t"

    echo -e "----\nPlaying \t [$sqlfile]\n-----" >> "$LOGFILE"
    psql -e -v ON_ERROR_STOP=on -U $DBUSER -h $DBSERVER -p $DBPORT -d $DBNAME -f "$sqlfile"  &>>"$LOGFILE" ; r=$?
    if [ $r -eq 0 ] 
    then
        psql -e -At -U $DBUSER -h $DBSERVER -p $DBPORT -d $DBNAME \
            -c "insert into played_scripts(name) values ('$(basename $sqlfile)')"  &>>"$LOGFILE" ; r=$?
        [ $r -eq 0 ] && echo "OK"
        [ $r -ne 0 ] && echo "KO (The script '$sqlfile' has been played but wasn't entered in table 'played_scripts')" && exit 1
    else
        echo "KO (Logfile is available in $LOGFILE)" && exit 1
    fi
done

