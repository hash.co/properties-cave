
-- propertyinfo
CREATE TABLE propertyinfo
(
    name text NOT NULL,
    description text,
    valuetype propertytype,
    domain domaintype,
    editor text,
    lastupdate timestamp with time zone DEFAULT now(),
    CONSTRAINT propertyinfo_pkey PRIMARY KEY (name),
    CONSTRAINT no_space_in_property_name CHECK (name !~ '[[:space:]]'::text)
);

alter type propertytype ADD VALUE 'folder';
alter type propertytype ADD VALUE 'ftp';
alter type propertytype ADD VALUE 'email';
ALTER TYPE propertytype add value 'infra';

alter type domaintype ADD VALUE 'install-iris';
alter type domaintype ADD VALUE 'neo';

INSERT INTO propertyinfo (name,description) 
    SELECT DISTINCT name, string_agg(description, ' | ') 
    FROM properties GROUP BY name ORDER BY name;

-- Modification on properties to create propertyvalue
ALTER TABLE properties RENAME TO propertyvalue;
ALTER TABLE propertyvalue ADD foreign key (name) REFERENCES propertyinfo(name);
ALTER TABLE propertyvalue DROP constraint no_space_in_property_name;
ALTER TABLE propertyvalue DROP description;
ALTER TABLE propertyvalue DROP valuetype;
ALTER TABLE propertyvalue DROP domain;

alter sequence properties_id_seq rename to propertyvalue_id_seq;

-- Properties view
CREATE VIEW properties AS 
SELECT pi.name, pv.value, pv.id, pv.platform, pv.version, pv.version_limit, pi.description, 
       pv.environment, pv.country, pv.editor, pv.lastupdate, pi.valuetype, pv.note, pv.template, pi.domain 
FROM propertyinfo pi 
JOIN propertyvalue pv using(name) ;


CREATE RULE properties_UPDATE AS ON UPDATE TO properties DO INSTEAD (
    UPDATE propertyinfo SET description=NEW.description, valuetype=NEW.valuetype, domain=NEW.domain WHERE name=OLD.name;
    UPDATE propertyvalue SET value=NEW.value,platform=NEW.platform, version=NEW.version, version_limit=NEW.version_limit, 
            environment=NEW.environment, country=NEW.country, editor=NEW.editor, lastupdate=NEW.lastupdate, note=NEW.note, template=NEW.template  
        WHERE id=OLD.id;
);

CREATE RULE properties_DELETE AS ON DELETE TO properties DO INSTEAD (
    DELETE FROM propertyvalue WHERE id=OLD.id;
);

CREATE OR REPLACE FUNCTION properties_insert_row() RETURNS TRIGGER AS $$
    DECLARE
        existingproperty propertyinfo;
    BEGIN
        SELECT * INTO existingproperty FROM propertyinfo where name = NEW.name;
        IF NOT FOUND THEN 
            INSERT INTO propertyinfo(name, description, valuetype, domain) 
                VALUES(NEW.name, NEW.description, NEW.valuetype, NEW.domain);
        END IF;
        INSERT INTO propertyvalue(name, value, platform, version, version_limit, environment, country, editor, lastupdate, note, template) 
            VALUES(NEW.name, NEW.value, NEW.platform, NEW.version, NEW.version_limit, NEW.environment, NEW.country, NEW.editor, NEW.lastupdate, NEW.note, NEW.template);

        RETURN NEW;
     END;
$$ LANGUAGE plpgsql;


CREATE TRIGGER properties_INSERT
    INSTEAD OF INSERT ON properties
    FOR EACH ROW
    EXECUTE PROCEDURE properties_insert_row();


