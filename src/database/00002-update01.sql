-- dwarfgarden.schema_update01.sql
-- Ticket JIRA EIB-3348
--
CREATE TYPE domaintype AS ENUM ('appli', 'install');
ALTER TABLE properties ADD COLUMN domain domaintype;
