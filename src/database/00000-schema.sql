--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;




SET search_path = public, pg_catalog;
SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: application; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE application (
    name text,
    description text,
    id integer NOT NULL,
    version character varying,
    server text
);


--
-- Name: application_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE application_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: application_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE application_id_seq OWNED BY application.id;

--
-- Name: country; Type: TABLE; Schema: public; Owner: iris; Tablespace: 
--

CREATE TABLE country (
    name text NOT NULL,
    description text
);


--
-- Name: country_pkey; Type: CONSTRAINT; Schema: public; Owner: iris; Tablespace: 
--

ALTER TABLE ONLY country
    ADD CONSTRAINT country_pkey PRIMARY KEY (name);


--
-- Name: environment; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE environment (
    name text NOT NULL,
    description text,
    role text,
    country text NOT NULL
);


--
-- Name: platform; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE platform (
    name text NOT NULL,
    description text,
    environment text NOT NULL,
    role text
);



--
-- Name: includes; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE includes (
    platform text NOT NULL,
    filename text NOT NULL,
    template text NOT NULL
);
ALTER TABLE ONLY includes
    ADD CONSTRAINT includes_pkey PRIMARY KEY (platform,filename);

--
-- Name: properties; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TYPE propertytype AS ENUM ('text', 'cron', 'url', 'list', 'number', 'boolean');

CREATE TABLE properties (
    name           text NOT NULL,
    value          text NOT NULL,
    id             integer NOT NULL,
    platform       text,
    version        text,
    version_limit  text,
    description    text,
    environment    text,
    country        text,
    editor         text,
    lastupdate     timestamp with time zone,
    valuetype      propertytype,
    note           text,
    template       text,
    CONSTRAINT non_empty_versions CHECK (((version <> ''::text) AND (version_limit <> ''::text)))
);


--
-- Name: properties_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE properties_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: properties_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE properties_id_seq OWNED BY properties.id;


--
-- Name: re_platform_server; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE re_platform_server (
    platform text NOT NULL,
    server text NOT NULL
);


--
-- Name: server; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE server (
    hostname text NOT NULL,
    ip character varying(15),
    description text
);

CREATE TYPE missingproperties AS
    (name text,
     platforms text[]);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY application ALTER COLUMN id SET DEFAULT nextval('application_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY properties ALTER COLUMN id SET DEFAULT nextval('properties_id_seq'::regclass);


--
-- Data for Name: application; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY application (name, description, id, version, server) FROM stdin;
\.


--
-- Name: application_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('application_id_seq', 1, false);


--
-- Data for Name: environment; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY environment (name, description, role, country) FROM stdin;
GDP	gdp	\N	FR
QUA	qualification	IRIS	FR
REC	user acceptance france	IRIS	FR
PROD	production	IRIS	FR
DEV	development	IRIS	FR
DEMO	demonstration	IRIS	FR
UAT	uat	IRIS	ES
ES_DEV	\N	IRIS	ES
ES_QUA	\N	IRIS	ES
ES_PROD	\N	IRIS	ES
\.



--
-- Data for Name: platform; Type: TABLE DATA; Schema: public; Owner: iris
--

COPY platform (name, description, environment, role) FROM stdin;
DEMO	\N	DEMO	\N
DEV1	\N	DEV	\N
DEV2	\N	DEV	\N
DEV3	\N	DEV	\N
GDP1	\N	GDP	\N
GDP2	\N	GDP	\N
PROD1	\N	PROD	\N
PROD2	\N	PROD	\N
PROD3	\N	PROD	\N
PROD4	\N	PROD	\N
QUA1	\N	QUA	\N
QUA2	\N	QUA	\N
REC1	\N	REC	\N
REC2	\N	REC	\N
DEV4	\N	ES_DEV	\N
DEV5	\N	ES_DEV	\N
DEV6	\N	ES_DEV	\N
UAT1	\N	UAT	\N
UAT2	\N	UAT	\N
ES_PROD1	\N	ES_PROD	\N
ES_QUA1	\N	ES_QUA	\N
\.



--
-- Data for Name: country; Type: TABLE DATA; Schema: public; Owner: iris
--

COPY country (name, description) FROM stdin;
FR	France
ES	Spain
\.


--
-- Data for Name: properties; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY properties (name, value, id, platform, version, version_limit, description, environment, country) FROM stdin;
\.



--
-- Data for Name: re_platform_server; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY re_platform_server (platform, server) FROM stdin;
\.


--
-- Data for Name: server; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY server (hostname, ip, description) FROM stdin;
prod-gdp2	172.16.156.37	\N
prod-gdp1	172.16.156.27	\N
rec-db2	172.16.156.31	db
rec-db1	172.16.156.13	db
qua-db2	172.16.156.50	db
qua-db1	172.16.156.52	db
prod-dbi2	172.16.156.49	db
prod-dbi1	172.16.156.51	db
dev-db6	192.168.211.26	db
dev-db5	192.168.211.25	db
dev-db4	192.168.211.24	db
dev-db3	192.168.111.23	db
dev-db2	192.168.111.22	db
dev-db1	192.168.111.21	db
rec-app2	172.16.156.30	app
rec-app1	172.16.156.12	app
qua-app2	172.16.156.47	app
qua-app1	172.16.156.18	app
prod-app4	172.16.156.44	app
prod-app3	172.16.156.39	app
prod-app2	172.16.156.23	app
prod-app1	172.16.156.24	app
dev-app6	192.168.211.16	app
dev-app5	192.168.211.15	app
dev-app4	192.168.211.14	app
dev-app3	192.168.111.13	app
dev-app2	192.168.111.12	app
dev-app1	192.168.111.11	app
rec-web2	172.16.156.32	web
rec-web1	172.16.156.14	web
qua-web2	172.16.156.48	web
qua-web1	172.16.156.20	web
prod-web4	172.16.156.46	web
prod-web3	172.16.156.41	web
prod-web2	172.16.156.21	web
prod-web1	172.16.156.26	web
dev-web6	192.168.211.36	web
dev-web5	192.168.211.35	web
dev-web4	192.168.211.34	web
dev-web3	192.168.111.33	web
dev-web2	192.168.111.32	web
dev-web1	192.168.111.31	web
demo-web1	172.16.156.34	web
\.


--
-- Name: application_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY application
    ADD CONSTRAINT application_pkey PRIMARY KEY (id);


--
-- Name: environment_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY environment
    ADD CONSTRAINT environment_pkey PRIMARY KEY (name);

--
-- Name: environment_country_fkey; Type: FK CONSTRAINT; Schema: public; Owner: iris
--

ALTER TABLE ONLY environment
    ADD CONSTRAINT environment_country_fkey FOREIGN KEY (country) REFERENCES country(name);

--
-- Name: platform_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY platform
    ADD CONSTRAINT platform_pkey PRIMARY KEY (name);


--
-- Name: properties_duplication; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY properties
    ADD CONSTRAINT properties_duplication UNIQUE (name, platform, environment, version);


--
-- Name: properties_country_fkey; Type: FK CONSTRAINT; Schema: public; Owner: iris
--

ALTER TABLE ONLY properties
    ADD CONSTRAINT properties_country_fkey FOREIGN KEY (country) REFERENCES country(name);


--
-- Name: properties_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY properties
    ADD CONSTRAINT properties_pkey PRIMARY KEY (id);


--
-- Name: re_platform_server_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY re_platform_server
    ADD CONSTRAINT re_platform_server_pkey PRIMARY KEY (platform, server);


--
-- Name: server_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY server
    ADD CONSTRAINT server_pkey PRIMARY KEY (hostname);


--
-- Name: application_server_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY application
    ADD CONSTRAINT application_server_fkey FOREIGN KEY (server) REFERENCES server(hostname);


--
-- Name: platform_environment_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY platform
    ADD CONSTRAINT platform_environment_fkey FOREIGN KEY (environment) REFERENCES environment(name);


--
-- Name: properties_environment_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY properties
    ADD CONSTRAINT properties_environment_fkey FOREIGN KEY (environment) REFERENCES environment(name);


--
-- Name: properties_platform_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY properties
    ADD CONSTRAINT properties_platform_fkey FOREIGN KEY (platform) REFERENCES platform(name);


--
-- Name: re_platform_server_platform_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY re_platform_server
    ADD CONSTRAINT re_platform_server_platform_fkey FOREIGN KEY (platform) REFERENCES platform(name);


--
-- Name: re_platform_server_server_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY re_platform_server
    ADD CONSTRAINT re_platform_server_server_fkey FOREIGN KEY (server) REFERENCES server(hostname);


ALTER TABLE ONLY includes
    ADD CONSTRAINT includes_platform_fkey FOREIGN KEY (platform) REFERENCES platform(name);


-- ---------------------------------
-- Compare versions as 
--     before <= tocompare < after
-- ---------------------------------
create or replace function version_between(tocompare text,before text,after text) returns boolean 
as $$
DECLARE
    greaterthanbefore boolean;
    smallerthanafter boolean;
BEGIN
    -- check the syntax of versions first 
    IF tocompare is null THEN
        RAISE EXCEPTION 'Cannot compare null version to anything';
    END IF;
    IF tocompare !~ E'^[\\d\.]+(-SNAPSHOT)?$' THEN
        RAISE EXCEPTION E'% doesn''t match ''^[\\d\.]+(-SNAPSHOT)?$'' ',tocompare;
    END IF;
    IF before is not null and before  !~ E'^[\\d\.]+(-SNAPSHOT)?$' THEN
        RAISE EXCEPTION E'% doesn''t match ''^[\\d\.]+(-SNAPSHOT)?$'' ', before;
    END IF;
    IF after is not null and after !~ E'^[\\d\.]+(-SNAPSHOT)?$' THEN
        RAISE EXCEPTION E'% doesn''t match ''^[\\d\.]+(-SNAPSHOT)?$'' ', after;
    END IF;

    -- check against 'before' first
    --  * check if 'before' is null => tocompare is greater than 'before'
    IF before is null THEN 
        greaterthanbefore = true;
    ELSE
        greaterthanbefore = regexp_split_to_array(substring(before, E'^[\\d\.]+'),E'\\.')::int[] <= regexp_split_to_array(substring(tocompare, E'^[\\d\.]+'),E'\\.')::int[];
    END IF;

    -- check against 'after'
    --  * if 'after' is null => tocompare is smaller than 'after'
    IF after is null THEN
        smallerthanafter = true;
    ELSE
        smallerthanafter = regexp_split_to_array(substring(tocompare, E'^[\\d\.]+'),E'\\.')::int[] < regexp_split_to_array(substring(after, E'^[\\d\.]+'),E'\\.')::int[];
    END IF;

    IF greaterthanbefore AND smallerthanafter THEN
        RETURN TRUE;
    ELSE
        RETURN FALSE;
    END IF;
END;
$$ language plpgsql;


--
-- PostgreSQL database dump complete
--

