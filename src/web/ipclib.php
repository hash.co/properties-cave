<?php
require_once('log4php/Logger.php');
if (file_exists('conf/config.php')) {
    require_once('conf/config.php');
}

class Postgres {

    private $db;
    private $logger;
    private $config;

    function __construct() {
        $this->logger = Logger::getLogger("Postgres");
        if (class_exists('Config')) {
            $this->config = Config::getInstance();
        }
    }

    private function connect() {

        $this->logger->debug("Connecting to DB ");

        if (!isset($this->config) || $this->config == null) {
            $this->logger->debug("is not set DB ==> ");
            $this->db = new PDO("pgsql:host=localhost;dbname=dwarfgarden", "postgres", "postgres");
        } else {
            $this->logger->debug("is set DB ==> ");
            $this->db = new PDO("pgsql:host=".$this->config->get('SERVER').";dbname=".$this->config->get('DATABASE'),
                 $this->config->get('USER'),
                 $this->config->get('PASSWORD'));
        }

        $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    public function selectData($sqlquery,$paramsMap) {
        if ($this->db == null) {
            $this->connect();
        }
        $this->logger->debug("Launching    request: ".$sqlquery);
        $dbh = $this->getDB();
        $dbh->beginTransaction();
        $stmt = $dbh->prepare($sqlquery);
        if ($paramsMap != null) {
            $this->logger->debug("    with  parameters: ".json_encode($paramsMap));
            foreach ($paramsMap as $key => $value) {
                $stmt->bindValue(':'.$key, $value);
            }
        }
        $stmt->execute();
        $datalist = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $dbh->commit();
        $datajson = json_encode($datalist);
        $this->logger->debug("Response: ".$datajson);
        return $datajson;
    }

    public function getDB() {
        if ($this->db == null) {
            $this->connect();
        }
        return $this->db;
    }
}

class IPCManager {
    private $db;
    private $body;
    private $parameters;
    private $logger;


    function __construct() {
        $this->db = new Postgres();
        $this->logger = Logger::getLogger("DwarfgardenManager");
    }

    public function setParameters($params,$payload) {
        $this->logger->debug("Setting parameters: ".json_encode($params ) );
        $this->parameters = $params;
        $this->logger->debug("Setting body: ".json_encode($payload));
        $this->body = $payload;
    }

    public function resetParameters() {
        $this->parameters = null;
        $this->body = null;
    }

    public function getPropertyInfoList() {
        $this->logger->debug("Get the propertyinfo list");
        $query = "SELECT * from propertyinfo order by name;";
        return $this->db->selectData($query,null);
    }

    public function getPropertyTypes() {
        $this->logger->debug("Get the propertytype list");
        $query = "SELECT unnest(enum_range(NULL::propertytype)) AS valuetype;";
        return $this->db->selectData($query,null);
    }

    public function getDomains() {
        $this->logger->debug("Get the domain list");
        $query = "SELECT unnest(enum_range(NULL::domaintype)) AS domaintype;";
        return $this->db->selectData($query,null);
    }

    public function getPlatforms() {
        $this->logger->debug("Get the platform list");
        $query = "select p.name as platform,e.name as environment, c.name as country
				from country c
				left join environment e on e.country = c.name and e.role = 'IRIS'
				left join platform p  on e.name = p.environment ;";
        return $this->db->selectData($query,null);
    }

    public function getIncludes() {
        $this->logger->debug("Get the includes list");
        $query = "select * from includes;";
        return $this->db->selectData($query,null);
    }

    public function getVersionList() {
        $query = "select version from (select distinct unnest(array_agg(distinct version) || array_agg(distinct version_limit)) as version
            from properties) u where u.version is not null order by u.version;";
        return $this->db->selectData($query,null);
    }

    public function addProperties() {
        $this->logger->debug("Adding the list of properties to the DB");

        $dbh = $this->db->getDB();
        if ($this->body != null) {
            $this->logger->debug("Size of the properties list : ".sizeof($this->body));
            $dbh->beginTransaction();
            $this->insertProp($this->body,$dbh);
            $dbh->commit();
        } else {
            $this->logger->debug("The list is empty");
        }
    }

    private function insertProp($properties,$dbh) {
        if (isset($_SERVER['PHP_AUTH_USER'])) {
            $username = $_SERVER['PHP_AUTH_USER'];
        } else {
            $username = null;
        }
        $sqlInsert="INSERT into properties (name,value,description,platform,environment,country,version,
            version_limit,editor,lastupdate,note,template,domain)
            values (:name,:value,:desc,:pf,:env,:country,:version,:version_limit,:editor,now(),:note,:template,:domain);";
        $stmtInsert = $dbh->prepare($sqlInsert);

        foreach ($properties as $prop) {
            $prop->editor = $username;
            $this->logger->info("adding propery : " . json_encode($prop));
            $stmtInsert->bindValue(':name',$prop->name);
            $stmtInsert->bindValue(':value',$prop->value);
            $stmtInsert->bindValue(':desc',$prop->description);
            if ($prop->platform) {
                $stmtInsert->bindValue(':pf',$prop->platform,PDO::PARAM_STR);
                $stmtInsert->bindValue(':env',null,PDO::PARAM_STR);
                $stmtInsert->bindValue(':country',null,PDO::PARAM_STR);
            } else if ($prop->environment){
                $stmtInsert->bindValue(':pf',null,PDO::PARAM_STR);
                $stmtInsert->bindValue(':env',$prop->environment,PDO::PARAM_STR);
                $stmtInsert->bindValue(':country',null,PDO::PARAM_STR);
            } else if ($prop->country){
                $stmtInsert->bindValue(':pf',null,PDO::PARAM_STR);
                $stmtInsert->bindValue(':env',null,PDO::PARAM_STR);
                $stmtInsert->bindValue(':country',$prop->country,PDO::PARAM_STR);
            } else {
                $stmtInsert->bindValue(':pf',null,PDO::PARAM_STR);
                $stmtInsert->bindValue(':env',null,PDO::PARAM_STR);
                $stmtInsert->bindValue(':country',null,PDO::PARAM_STR);
            }
            $stmtInsert->bindValue(':domain',$prop->domain == '' ? null : $prop->domain,PDO::PARAM_STR);
            $stmtInsert->bindValue(':version',$prop->version == '' ? null : $prop->version,PDO::PARAM_STR);
            $stmtInsert->bindValue(':version_limit',$prop->version_limit == '' ? null : $prop->version_limit,PDO::PARAM_STR);
            $stmtInsert->bindValue(':editor', $prop->editor);
            $stmtInsert->bindValue(':note', $prop->note);
            $stmtInsert->bindValue(':template', $prop->template);
            $stmtInsert->execute();
        }
        $this->logger->debug("Finished adding the properties.");
    }

    private function updateProp($prop,$dbh) {
        $sqlUpdate="UPDATE properties set name=:name, value=:value, description=:description, version=:version,
        version_limit=:version_limit, editor=:editor, lastupdate=now(), template=:template, platform=:pf, environment=:env,
        country=:country,domain=:domain where id=:id";

        $this->logger->debug("Processing : " . json_encode($prop));
        $stmtUpdate = $dbh->prepare($sqlUpdate);
        $stmtUpdate->bindValue(':name', $prop->name);
        $stmtUpdate->bindValue(':value', $prop->value);
        $stmtUpdate->bindValue(':description', $prop->description);
        $stmtUpdate->bindValue(':domain', $prop->domain);
        $stmtUpdate->bindValue(':version', $prop->version == '' ? null : $prop->version ,PDO::PARAM_STR);
        $stmtUpdate->bindValue(':version_limit', $prop->version_limit == '' ? null : $prop->version_limit,PDO::PARAM_STR);
        $stmtUpdate->bindValue(':editor',$prop->editor);
        $stmtUpdate->bindValue(':template',$prop->template);
        $stmtUpdate->bindValue(':id',$prop->id,PDO::PARAM_INT);
        if ($prop->platform) {
            $stmtUpdate->bindValue(':pf',$prop->platform,PDO::PARAM_STR);
            $stmtUpdate->bindValue(':env',null,PDO::PARAM_STR);
            $stmtUpdate->bindValue(':country',null,PDO::PARAM_STR);
        } else if ($prop->environment){
            $stmtUpdate->bindValue(':pf',null,PDO::PARAM_STR);
            $stmtUpdate->bindValue(':env',$prop->environment,PDO::PARAM_STR);
            $stmtUpdate->bindValue(':country',null,PDO::PARAM_STR);
        } else if ($prop->country){
            $stmtUpdate->bindValue(':pf',null,PDO::PARAM_STR);
            $stmtUpdate->bindValue(':env',null,PDO::PARAM_STR);
            $stmtUpdate->bindValue(':country',$prop->country,PDO::PARAM_STR);
        } else {
            $stmtUpdate->bindValue(':pf',null,PDO::PARAM_STR);
            $stmtUpdate->bindValue(':env',null,PDO::PARAM_STR);
            $stmtUpdate->bindValue(':country',null,PDO::PARAM_STR);
        }
        $stmtUpdate->execute();

        $this->logger->debug("Finished updating the property.");
    }

    public function modifyProp() {
        $this->logger->debug("Modify property ");
        if (isset($_SERVER['PHP_AUTH_USER'])) {
            $username = $_SERVER['PHP_AUTH_USER'];
        } else {
            $username = null;
        }
        $dbh = $this->db->getDB();
        if ($this->body != null) {
            $prop = $this->body;
            $prop->editor = $username;

            $this->logger->info("Property to update : ".json_encode($this->body));
            $dbh->beginTransaction();

            if ($prop->updatevalueforversion) {
                $sqlUpdate="UPDATE properties set version_limit=:version_limit, editor=:editor, lastupdate=now() where id=:id";
                $stmtUpdate = $dbh->prepare($sqlUpdate);
                $stmtUpdate->bindValue(':version_limit', $prop->changevalueversion == '' ? null : $prop->changevalueversion,PDO::PARAM_STR);
                $stmtUpdate->bindValue(':editor',$prop->editor);
                $stmtUpdate->bindValue(':id',$prop->id,PDO::PARAM_INT);
                $stmtUpdate->execute();

                $oldversion = $prop->version;
                $prop->version = $prop->changevalueversion;
                $this->insertProp(array($prop),$dbh);
            } else {
                $this->updateProp($prop, $dbh);
            }

            $dbh->commit();
        } else {
            $this->logger->info("The property sent is empty");
        }
    }

    public function removeProperty() {
        $this->logger->debug("Delete property ");
        if (isset($_SERVER['PHP_AUTH_USER'])) {
            $username = $_SERVER['PHP_AUTH_USER'];
        } else {
            $username = null;
        }
        if ($this->parameters['propid'] == null) {
            $this->logger->info("The property sent is empty");
            return '{}';
        } else {
            $dbh = $this->db->getDB();
            $prop = $this->db->selectData("select * from properties where id = :id ",array('id' => $this->parameters['propid']));
            $this->logger->info("Property to remove : ".$prop);
            $dbh->beginTransaction();
            $sqlUpdate="DELETE FROM properties where id=:id";
            $stmtUpdate = $dbh->prepare($sqlUpdate);
            $stmtUpdate->bindValue(':id',$this->parameters['propid'],PDO::PARAM_INT);
            $stmtUpdate->execute();
            $dbh->commit();
            $this->logger->debug("Finished removing the property.");
        }
    }

    public function getPropertiesInfo() {
        $this->logger->debug("Getting properties infomation for property: ".$this->parameters['name']);
        if ($this->parameters['name'] == null) {
            return '{}';
        }
        else {
            $query = "select p.id, p.name,value,domain,version,version_limit,platform,coalesce(p.environment, pf.environment) as environment, coalesce(p.country, e1.country, e.country) as country,
            editor, lastupdate::date, template
            from properties p
            left join platform pf on pf.name = p.platform
            left join environment e on e.name = pf.environment
            left join environment e1 on e1.name = p.environment
            where p.name = :name order by p.country, environment, p.platform, version;";
            return $this->db->selectData($query,array('name' => $this->parameters['name']));
        }

    }

    public function getMissingProperties() {
        $this->logger->debug("Listing the missing properties for version: " .$this->parameters['version'] );
        if ($this->parameters['version'] == null) {
            return '[]';
        } else {
            if ($this->parameters['country'] == 'none') {
                $query = "select * from listMissingProperties(:version) order by name";
                return $this->db->selectData($query,array('version' => $this->parameters['version']));
            }
            else {
                $query = "select * from listMissingProperties(:version, :country) order by name";
                return $this->db->selectData($query,array('version' => $this->parameters['version'], 'country' => $this->parameters['country'] ));
            }

        }
    }

    public function getReusedProperties() {
        $this->logger->debug("Listing all the re-used properties ");
        $query = "select * from listReusedProperties() order by name";
        return $this->db->selectData($query);
    }

    public function getPlatformProperties() {
        $this->logger->debug("Listing the platform properties for platform '".$this->parameters['platform']."' and version '".(isset($this->parameters['version']) ? $this->parameters['version'] : null )."'");
        if ($this->parameters['platform'] == null) {
            return '[]';
        } else if (!isset($this->parameters['version']) || $this->parameters['version'] == null) {
            $query = "select * from listplatformproperties(:platform) order by name, version_limit";
            $query_params = array('platform' => $this->parameters['platform']);
        } else {
            $query = "select * from listplatformproperties(:platform, :version) order by name, version_limit";
            $query_params = array('platform' => $this->parameters['platform'], 'version' => $this->parameters['version']);
        }
         return $this->db->selectData($query,$query_params);
    }

    public function getPlatformPropertiesFile() {
        $this->logger->debug("Generating platform properties file content for platform '".$this->parameters['platform']."' and version '".(isset($this->parameters['version']) ? $this->parameters['version'] : null )."'");
        if ($this->parameters['platform'] == null || !isset($this->parameters['version']) || $this->parameters['version'] == null) {
            return '[]';
        }
        if (isset($this->parameters['useInclude']) && $this->parameters['useInclude'] == "false") {
            $this->logger->debug("We won't use the '@Include template.conf'" );
            $query = "select name || '=' || value as line from listplatformproperties(:platform, :version)";
        } else {
            $query = "select * from generatePropertiesFile(:platform, :version) as a(line)";
        }
        $query_params = array('platform' => $this->parameters['platform'], 'version' => $this->parameters['version']);
        $json_data = $this->db->selectData($query, $query_params);
        $data_table = json_decode($json_data);
        $data_text = "";
        foreach ($data_table as $value) {
            $data_text = $data_text.$value->{"line"}."\n";
        }
        return $data_text;
    }

    public function modifyPropertyInfo() {
        $this->logger->debug("Modify property ");
        if (isset($_SERVER['PHP_AUTH_USER'])) {
            $username = $_SERVER['PHP_AUTH_USER'];
        } else {
            $username = null;
        }
        $dbh = $this->db->getDB();
        if ($this->body != null) {
            $prop = $this->body;
            $prop->editor = $username;

            $this->logger->info("Property to update : ".json_encode($this->body));
            $dbh->beginTransaction();

            $sqlUpdate="UPDATE propertyinfo set description=:description, editor=:editor, lastupdate=now(), domain=:domain, valuetype=:valuetype
                where name=:name";
            $stmtUpdate = $dbh->prepare($sqlUpdate);
            $stmtUpdate->bindValue(':valuetype',$prop->valuetype);
            $stmtUpdate->bindValue(':description',$prop->description);
            $stmtUpdate->bindValue(':domain',$prop->domain);
            $stmtUpdate->bindValue(':editor',$prop->editor);
            $stmtUpdate->bindValue(':name',$prop->name);
            $stmtUpdate->execute();

            $dbh->commit();
        } else {
            $this->logger->info("The property sent is empty");
        }
    }

    public function removePropertyinfo() {
        $this->logger->debug("Delete property ");
        if (isset($_SERVER['PHP_AUTH_USER'])) {
            $username = $_SERVER['PHP_AUTH_USER'];
        } else {
            $username = null;
        }
        if ($this->parameters['propname'] == null) {
            $this->logger->info("The property sent is empty");
            return '{}';
        } else {
            $dbh = $this->db->getDB();
            $prop = $this->db->selectData("select * from propertyinfo where name = :name ",array('name' => $this->parameters['propname']));
            $this->logger->info("Property to remove : ".$prop);
            $dbh->beginTransaction();
            $sqlUpdate="DELETE FROM propertyinfo where name=:name";
            $stmtUpdate = $dbh->prepare($sqlUpdate);
            $stmtUpdate->bindValue(':name',$this->parameters['propname'],PDO::PARAM_INT);
            $stmtUpdate->execute();
            $dbh->commit();
            $this->logger->debug("Finished removing the property.");
        }
    }
}
?>
