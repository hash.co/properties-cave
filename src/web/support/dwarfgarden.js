/**
 *Déclaration de l'application ipcApp
 */
var ipcApp = angular.module('ipcApp', ['ngResource', 'ipcServices', 'ipcControllers']);
var ipcServices = angular.module('ipcServices', ['ngTable','ngResource']);
var ipcControllers = angular.module('ipcControllers', ['ngTable','ui.bootstrap','ui.router','dialogs.main']);

// support/ipc.js
'use strict';

ipcControllers.config(['$stateProvider', '$urlRouterProvider',
    function ($stateProvider,   $urlRouterProvider) {
        $urlRouterProvider.when('', '/rechercher');

        $stateProvider.state("rechercher", {
            url: '/rechercher',
            templateUrl: 'pages/recherche.html',
            controller : 'ipcCtrl'
        })
        .state("ajouter",{
            url: '/ajouter',
            templateUrl: 'pages/ajout.html',
            controller : 'ipcCtrl'
        })
        .state("verifier", {
            url: '/verifier',
            templateUrl:'pages/checkproperties.html',
            controller : 'ipcCtrl'
        })
        .state("lister", {
            url: '/lister',
            templateUrl:'pages/list.html',
            controller : 'listPropertiesCtrl'
        });
    }]);


ipcServices.factory('propertieschecker', ['$resource',
    function ($resource) {
        return $resource('/cgi-bin/check-properties-vs-packages-web.sh', {}, {
            checkvspackages: {
                method: 'GET',
                headers: {'Accept-Encoding':''},
            }
        });
}]);

ipcServices.factory('dgserver', ['$resource',
    function ($resource) {
        return $resource('ipc.php', {}, {
            listdomains: {
                method: 'GET',
                params: {
                    action: 'getDomains'
                },
                isArray: true
            },
            listproptypes: {
                method: 'GET',
                params: {
                    action: 'getPropertyTypes'
                },
                isArray: true
            },
            listpf: {
                method: 'GET',
                params: {
                    action: 'getPlatforms'
                },
                isArray: true
            },
            listversions: {
                method: 'GET',
                isArray: true,
                params: {
                    action: 'getVersionList'
                }
            },
            addprops: {
                method: 'PUT',
                params: {
                    action: 'addProperties'
                }
            },
            modify: {
                method: 'POST',
                params: {
                    action: 'modifyProp',
                }
            },
            propinfo: {
                method: 'GET',
                isArray: true,
                params: {
                    action: 'getPropertiesInfo'
                }
            },
            listmissingprops: {
                method: 'GET',
                isArray: true,
                params: {
                    action: 'getMissingProperties'
                }
            },
            listprops: {
                method: 'GET',
                isArray: true,
                params: {
                    action: 'getPlatformProperties'
                }
            },
            remove: {
                method: 'DELETE',
                params: {
                    action: 'removeProperty'
                }
            },
            getIncludes: {
                method: 'GET',
                isArray: true,
                params: {
                    action: 'getIncludes'
                }
            },
            getpropsfilecontent: {
                method: 'GET',
                isArray: true,
                params: {
                    action: 'getPlatformPropertiesFile'
                }
            },
            listpropertiesinfo: {
                method: 'GET',
                isArray: true,
                params: {
                    action: 'getPropertyInfoList'
                }
            },
            modifyPropertyinfo : {
                method: 'POST',
                params: {
                    action: 'modifyPropertyInfo',
                }
            },
            removePropertyinfo: {
                method: 'DELETE',
                params: {
                    action: 'removePropertyinfo'
                }
            },
            listReusedProperties: {
                method: 'GET',
                isArray: true,
                params: {
                    action: 'listReusedProperties'
                }
            }
        });
}]);

ipcServices.factory('fileservice', ['$resource', function() {

    var genDwarf = function(lines) {
        var content = "";
        if (lines) {
            $.each(lines,function(index,c) {
                content += c.line + "%0A";
            });
        }
        return content;
    };

    var genCSV = function(properties) {
        var content = "";
        var line = "";
        line += "\"name\"|";
        line += "\"value\"|";
        line += "\"description\"|";
        line += "\"version\"|";
        line += "\"version_limit\"|";
        line += "\"platform\"|";
        line += "\"environment\"|";
        line += "\"country\"|";
        line += "\"domain\"|";
        content += line + "%0A";
        if (properties) {
            $.each(properties,function(index,prop) {
                var line = "";
                line += "\"" + prop.name + "\"|";
                line += "\"" + prop.value + "\"|";
                line += "\"" + prop.description + "\"|";
                line += "\"" + prop.version + "\"|";
                line += "\"" + prop.version_limit + "\"|";
                line += "\"" + prop.platform + "\"|";
                line += "\"" + prop.environment + "\"|";
                line += "\"" + prop.country + "\"|";
                line += "\"" + prop.domain + "\"|";
                content += line + "%0A";
            });
        }
        return content;
    };

    var downloadFile = function(filename,content) {
        var dataUrl = 'data:text/plain;utf-8,' + content;
        var link = $('#downloadDwarfLink');
        angular.element(link)
          .attr('href', dataUrl)
          .attr('download', filename)
          .attr('target','_blank');
        (link[0] || link).click();
    };

    return {
        exportDwarf : function (lines,filename) {
                        var content = genDwarf(lines);
                        downloadFile(filename,content);
                    },
        exportCSV : function(properties, filename) {
                        var content = genCSV(properties);
                        downloadFile(filename,content);
                    }
    };
}]);

ipcServices.factory('uimanager', ['dgserver', function(dgserver){
    var listversions = function() {
        return dgserver.listversions({},function(injectedList) {
            var versions = [];
            $.each(injectedList,function () {
                versions.push(this.version);
            });
            return versions;
        });
    };

    var updateProperty = function(prop, editedProperties, OK_Callback) {
        var response = {};
        if (prop.environment == 'all') {
            prop.environment = null;
        }
        if (prop.platform == 'all') {
            prop.platform = null;
        }
        if (prop.country == 'all') {
            prop.country = null;
        }
        if (prop.updatevalueforversion && (!prop.changevalueversion || prop.changevalueversion == null || prop.changevalueversion == '')) {
            response.succeeded = false;
            response.servererror = {data : "La version de changement est obligatoire dans le cas d'un changement à partir d'une version."};
        } else {
            dgserver.modify({},prop,function() {
                editedProperties[prop.id] = null;
                OK_Callback();
                response.succeeded = true;
                response.servererror = null;
            }, function(errOut) {
                console.log(errOut);
                response.succeeded = false;
                response.servererror = errOut;
            });
        }
        return response;
    };

    var removeProperty = function(prop, editedProperties, OK_Callback) {
        console.log("Removing property with id " + prop.id);
        var response = {};
        dgserver.remove({propid : prop.id},function() {
            editedProperties[prop.id] = null;
            OK_Callback();
            response.succeeded = true;
            response.servererror = null;
        }, function(errOut) {
            console.log(errOut);
            response.succeeded = false;
            response.servererror = errOut;
        });
        return response;
    };

    var listEnvAndPf = function() {
        var pfinfos = dgserver.listpf({}, function(injectedPL) {
            var allinfosList = envManager(injectedPL);
            return allinfosList;
        });
        return pfinfos;
    };

    var invalidClass = function(propertyValue) {
        if (propertyValue == null || propertyValue == '')
            return 'has-warning';
        else
            return '';
    };

    var getTemplateOrValue = function(prop) {
        if (prop.template) {
            prop.value = '# Included in \'' + prop.template + '\' #';
        } else if (prop.template == '') {
            prop.template = null;
            prop.value = null;
        }
    };

    var envManager = function (pfl) {
        var platformlist = pfl;
        var envList = [];
        var countryList = [];
        var domainList = dgserver.listdomains({});
        var pfToEnvMap = {};

        var envListArr = [];
        var countryListArr = [];

        for (var i = 0; i < pfl.length; i++) {
            if ($.inArray(pfl[i].environment, envListArr) == -1) {
                envListArr.push(pfl[i].environment);
            }
            if ($.inArray(pfl[i].country, countryListArr) == -1) {
                countryListArr.push(pfl[i].country);
            }
             pfToEnvMap[pfl[i].platform] = pfl[i];
        }

        for (var i = 0; i < countryListArr.length; i++) {
            countryList.push({
                name: countryListArr[i]
            });
        }
        for (var i = 0; i < envListArr.length; i++) {
            envList.push({
                name: envListArr[i]
            });
        }



        var getEnvList = function(country) {
            envList = [];
            if (country != 'none') {
                var pfl = platformlist;
                for (var i = 0; i < pfl.length; i++) {
                    if (pfl[i].country == country) {
                        alreadyset = false;
                        for (var j = 0 ; j < envList.length ; j++) {
                            if (envList[j].name == pfl[i].environment) {
                                alreadyset = true;
                                break;
                            }
                        }
                        if (!alreadyset) {
                            envList.push({
                                name: pfl[i].environment
                            });
                        }
                    }
                }
            }
            return envList;
        };

        var getEnvPflist = function (env) {
            var plist = [];
            if (env != 'none') {
                var pfl = platformlist;
                for (var i = 0; i < pfl.length; i++) {
                    if (pfl[i].environment == env) {
                        plist.push({
                            name: pfl[i].platform
                        });
                    }
                }
            }
            return plist;
        };

        var versionsList = [];
        var listversions = function() {
            dgserver.listversions({},function(injectedList) {
                $.each(injectedList,function () {
                    versionsList.push(this.version);
                });
            });
        };

        var propertytypeList = [];
        var listpropertytype = function() {
            dgserver.listproptypes({},function(injectedList) {
                $.each(injectedList,function () {
                    propertytypeList.push(this.valuetype);
                });
            });
        };

        var templateList = [];
        var getTemplates = function() {
            dgserver.getIncludes({},function(injectedIncludes) {
                for (var i = 0; i < injectedIncludes.length; i++) {
                    if ($.inArray(injectedIncludes[i].template, templateList) == -1) {
                        templateList.push(injectedIncludes[i].template);
                    }
                }
            });
            return templateList;
        };

        return {
            platformlist : platformlist,
            envList : envList,
            domainList : domainList,
            countryList : countryList,
            versionsList : versionsList,
            templateList : templateList,
            pfToEnvMap : pfToEnvMap,
            getEnvPflist : getEnvPflist,
            getEnvList : getEnvList,
            listversions : listversions,
            getTemplates : getTemplates,
            propertytypeList : propertytypeList,
            listpropertytype : listversions,
        };
    };

    return {
        listEnvAndPf : listEnvAndPf,
        listversions : listversions,
        updateProperty : updateProperty,
        removeProperty : removeProperty,
        getEnvManager : envManager,
        invalidClass : invalidClass,
        getTemplateOrValue : getTemplateOrValue
    };
}]);

ipcServices.factory('ngTableParamBuilder', ['$filter','ngTableParams', function ($filter, ngTableParams) {
    var build = function(propertiesList,nbperpage) {
        var ngtableparams = new ngTableParams({
                page: 1, // show first page
                count: nbperpage ? nbperpage : propertiesList
            }, {
                total: function () {
                    var total = propertiesList ? propertiesList : 0;
                    return total;
                },
                getData: function ($defer, params) {
                    if (propertiesList) {
                        // When the user deletes the search text, this object keeps a variable with an empty string. The
                        // strict comparator will accept only empty strings. To overcome this problem we delete the property
                        for (var propt in params.filter()) {
                            if (params.filter()[propt] === '') {
                              delete params.filter()[propt];
                            }
                        }

                        var filteredData = params.filter() ? $filter('filter')(propertiesList, params.filter()) : propertiesList;
                        params.total(filteredData.length);
                        var pageData = filteredData.slice((params.page()-1) * params.count(),params.page() * params.count());
                        if (pageData.length == 0 && params.page() > 1 ) {
                            params.page(params.page() -1 );
                            pageData = filteredData.slice((params.page()-1) * params.count(),params.page() * params.count());
                        }
                        $defer.resolve(pageData);
                    }
                }
            });
        return {
            create : build
        };
    };

} ]);

/***********************************************************************
*   Controllers
************************************************************************/

ipcControllers.controller('menuCtrl', ['$scope', '$location', function($scope,$location) {
    $scope.activateMenu = function(page)
    {
        var currentRoute = $location.path().substring(1) || 'rechercher';
        return page === currentRoute ? 'active' : '';
    };

}]);

//Déclaration du "controller"
ipcControllers.controller('ipcCtrl', ['$scope', '$filter', '$modal', '$http', 'ngTableParams', 'uimanager','dgserver','dialogs', 'fileservice',
    function ($scope, $filter, $modal, $http, ngTableParams, uimanager, dgserver, dialogs, fileservice)
      {

        $scope.launchConfirmDialog = function (properties, OnOK_fct, OnKO_fct) {
            var dlg = dialogs.create('pages/propertiesListing.html','confirmDialogCtrl',{'properties' : properties},'lg');
            dlg.result.then(OnOK_fct,OnKO_fct);
        };

        $scope.downloadDwarf = function(platform, version) {
            dgserver.getpropsfilecontent({'platform': platform, 'version': version, 'comments': true}, function(lines){
                fileservice.exportDwarf(lines, platform + "_" + version + ".iris");
            }, function(errOut){
                console.log(errOut);
                $scope.serverresponse.servererror=errOut
            });
        };

        $scope.downloadCSV = function(props, platform, version) {
            fileservice.exportCSV(props, platform + "_" + version + ".csv");
        };
        $scope.downloadDwarfWOComments = function(platform, version) {
            dgserver.getpropsfilecontent({'platform': platform, 'version': version}, function(lines){
                fileservice.exportDwarf(lines, platform + "_" + version + ".iris");
            }, function(errOut){
                console.log(errOut);
                $scope.serverresponse.servererror=errOut
            });

        };

        var versions = null;
        $scope.tempid = 0;

        $scope.selectedCountry = 'none';
        $scope.selectedEnv = 'all';
        $scope.selectedPf = 'all';
        $scope.pflist = [];

        $scope.listversions = function() {
            dgserver.listversions({},function(injectedList) {
                $scope.versionsList = [];
                $.each(injectedList,function () {
                    $scope.versionsList.push(this.version);
                });

            });
        };

        $scope.listEnvAndPf = function() {
            dgserver.listpf({}, function(injectedPL) {
                envManager(injectedPL);
            });

            dgserver.getIncludes({},function(injectedIncludes) {
                $scope.templateList = [];
                $scope.includes = injectedIncludes;
                for (var i = 0; i < injectedIncludes.length; i++) {
                    if ($.inArray(injectedIncludes[i].template, $scope.templateList) == -1) {
                        $scope.templateList.push(injectedIncludes[i].template);
                    }
                }
            });
        };


        envManager = function (pfl) {
            $scope.platformlist = pfl;
            var envListArr = [];
            var countryListArr = [];

            $scope.envList = [];
            $scope.countryList = [];
            $scope.pfToEnvMap = {};

            $scope.domainList = dgserver.listdomains({});

            for (var i = 0; i < pfl.length; i++) {
                if ($.inArray(pfl[i].environment, envListArr) == -1) {
                    envListArr.push(pfl[i].environment);
                }
                if ($.inArray(pfl[i].country, countryListArr) == -1) {
                    countryListArr.push(pfl[i].country);
                }
                 $scope.pfToEnvMap[pfl[i].platform] = pfl[i];
            }

            for (var i = 0; i < countryListArr.length; i++) {
                $scope.countryList.push({
                    name: countryListArr[i]
                });
            }
            for (var i = 0; i < envListArr.length; i++) {
                $scope.envList.push({
                    name: envListArr[i]
                });
            }

        };


        $scope.invalidClass = function(propertyValue) {
            if (propertyValue == null || propertyValue == '')
                return 'has-warning';
            else
                return '';
        };

        $scope.getEnvList = function(country) {
            envList = [];
            if (country != 'none') {
                var pfl = $scope.platformlist;
                for (var i = 0; i < pfl.length; i++) {
                    if (pfl[i].country == country) {
                        alreadyset = false;
                        for (var j = 0 ; j < envList.length ; j++) {
                            if (envList[j].name == pfl[i].environment) {
                                alreadyset = true;
                                break;
                            }
                        }
                        if (!alreadyset) {
                            envList.push({
                                name: pfl[i].environment
                            });
                        }
                    }
                }
            }
            return envList;
        };

        $scope.getPflist = function () {
            if ($scope.selectedEnv != 'none') {
                $scope.pflist = [];
                var pfl = $scope.platformlist;
                for (var i = 0; i < pfl.length; i++) {
                    if (pfl[i].environment == $scope.selectedEnv) {
                        $scope.pflist.push({
                            name: pfl[i].platform
                        });
                    }
                }
            }
        };

        $scope.getEnvPflist = function (env) {
            var plist = [];
            if (env != 'none') {
                var pfl = $scope.platformlist;
                for (var i = 0; i < pfl.length; i++) {
                    if (pfl[i].environment == env) {
                        plist.push({
                            name: pfl[i].platform
                        });
                    }
                }
            }
            return plist;
        };

        $scope.removePropFromList = function(prop, propslist) {
            console.log("To remove : " + prop.tempid + " => " + prop.name);
            // find the index of prop
            var i = 0;
            var indextoremove = null;
            for(var i = 0; i < propslist.length; i++) {
                if (propslist[i].tempid == prop.tempid) {
                    indextoremove = i;
                    break;
                }
            }
            propslist.splice(i,1);
        };

        $scope.addNewProperties = function () {
            if ($scope.propertiesList != null && $scope.propertiesList != "") {
                if (!$scope.propertiesArray) {
                    $scope.propertiesArray = [];
                }
                var lines = $scope.propertiesList.split('\n')
                var lastdescription = null;
                $.each(lines, function () {
                    if (this == "") {
                        return;
                    } else if (this.indexOf('#') == 0) {
                        // manage a description
                        if (lastdescription) {
                            lastdescription += '\n' + this.substr(1);
                        } else {
                            lastdescription = this.substr(1);
                        }
                    } else {
                        var indexOfEquals = this.search('=');
                        if ( indexOfEquals == 0 ) {
                            return;
                        }
                        var propertyName;
                        var propertyValue;
                        if ( indexOfEquals > 0 ) {
                            propertyName  = this.substring(0, indexOfEquals);
                            propertyValue = this.substring(indexOfEquals + 1, this.length);
                        } else {
                            propertyName  = this.toString();
                        }
                        $scope.propertiesArray.push({
                                tempid: $scope.tempid,
                                name:  propertyName,
                                value: propertyValue,
                                environment: $scope.selectedEnv,
                                platform: $scope.selectedPf,
                                country: $scope.selectedCountry,
                                description: lastdescription,
                                version: $scope.version,
                                endversion: $scope.endversion
                            });
                        lastdescription = null;
                    }
                    $scope.tempid++;
                });
                $scope.propertiesList = null;
                $scope.enteredpropsParams.reload();
            }
        };

        $scope.saveProperties = function (propslist, onsuccess_fct) {

            $.each(propslist, function() {
                if (this.environment == 'all') {
                    this.environment = null;
                }
                if (this.platform == 'all') {
                    this.platform = null;
                }
                if (this.country == 'all') {
                    this.country = null;
                }
            });

            dgserver.addprops({},propslist,function() {
                propslist.length = 0;
                $scope.servererror=false;
                onsuccess_fct();
            },function(errOut){
                console.log(errOut);
                $scope.servererror=errOut;
                $.each(propslist, function() {
                    if (this.environment == null) {
                        this.environment = 'all';
                    }
                    if (this.platform == null) {
                        this.platform = 'all';
                    }
                    if (this.country == null) {
                        this.country = 'all';
                    }
                });
            });
        };

        $scope.showPropInfo = function(prop) {
            $scope.serverresponse = {};
            var propinfo = dgserver.propinfo({name:prop.name}, function() {
                $scope.serverresponse.servererror=null;
                $scope.selectedPropInfo = { name: prop.name,
                                           description: prop.description,
                                           valuetype: prop.valuetype,
                                           domain: prop.domain,
                                           valuesList: propinfo};
                console.log($scope.selectedPropInfo);
                var modalInstance = $modal.open({
                    templateUrl: 'pages/property-info.html',
                    controller: 'InfosModalCtrl',
                    size: 'lg',
                    resolve: {
                        infos: function () {
                            return $scope.selectedPropInfo;
                        }
                    }
                });
            }, function(errOut) {
                console.log(errOut);
                $scope.serverresponse.servererror=errOut
            });

        };

        // If a value is added or removed from the array
        $scope.$watch("propertiesArray.length", function () {
            $scope.enteredpropsParams.reload();
        });
        $scope.enteredpropsParams = new ngTableParams({
                page: 1, // show first page
                count: 100
            }, {
                total: function () {
                    var total = $scope.propertiesArray ? $scope.propertiesArray.length : 0;
                    return total;
                },
                getData: function ($defer, params) {
                    if ($scope.propertiesArray) {
                        // When the user deletes the search text, this object keeps a variable with an empty string. The
                        // strict comparator will accept only empty strings. To overcome this problem we delete the property
                        for (var propt in params.filter()) {
                            if (params.filter()[propt] === '') {
                              delete params.filter()[propt];
                            }
                        }

                        var filteredData = params.filter() ? $filter('filter')($scope.propertiesArray, params.filter()) : $scope.propertiesArray;
                        params.total(filteredData.length);
                        var pageData = filteredData.slice((params.page()-1) * params.count(),params.page() * params.count());
                        if (pageData.length == 0 && params.page() > 1 ) {
                            params.page(params.page() -1 );
                            pageData = filteredData.slice((params.page()-1) * params.count(),params.page() * params.count());
                        }
                        $defer.resolve(pageData);
                    }
                }
            }
        );
        /////////////////////////////////
        // checkproperties
        /////////////////////////////////
        $scope.missingprops = null;

        $scope.listMissingProperties = function () {
            console.log("Listing missing properties for version " + $scope.selectedVersion);
            $scope.checkingInProgress = true;
            var mprops = dgserver.listmissingprops({version : $scope.selectedVersion, country : $scope.selectedCountry}, function() {
                $scope.servererror=null;
                if (mprops.length == 0) {
                    $scope.infoNotification = "Aucune propriété à afficher.";
                    $scope.missingprops = null;
                } else {
                    $scope.infoNotification = null;
                    $scope.missingprops = [];
                    $.each(mprops, function() {
                        var pname = this.name;
                        var platforms = this.platforms.substring(1,this.platforms.length - 1);
                        var platformsArr = platforms.split(',');
                        $.each(platformsArr, function() {
                            console.log(pname + " => " + this);
                            $scope.missingprops.push({
                                tempid : $scope.tempid,
                                name : pname,
                                country: $scope.pfToEnvMap[this].country,
                                platform : this.toString(),
                                environment : $scope.pfToEnvMap[this].environment,
                                version: $scope.selectedVersion
                            });
                            $scope.tempid++;
                        });
                    });
                }
                $scope.checkpropsParams.reload();
                $scope.checkingInProgress = false;
            }, function(errOut){
                console.log(errOut);
                $scope.servererror=errOut;
                $scope.checkingInProgress = false;
            });

        };

        $scope.checkpropsParams = new ngTableParams({
                page: 1, // show first page
                count: 100
            }, {
                total: function () {
                    var total = $scope.missingprops ? $scope.missingprops.length : 0;
                    return total;
                },
                getData: function ($defer, params) {
                    if ($scope.missingprops) {
                        // When the user deletes the search text, this object keeps a variable with an empty string. The
                        // strict comparator will accept only empty strings. To overcome this problem we delete the property
                        for (var propt in params.filter()) {
                            if (params.filter()[propt] == '') {
                              delete params.filter()[propt];
                            }
                        }
                        var filteredData = params.filter() ? $filter('filter')($scope.missingprops, params.filter()) : $scope.missingprops;
                        params.total(filteredData.length);
                        var pageData = filteredData.slice((params.page()-1) * params.count(),params.page() * params.count());
                        if (pageData.length == 0 && params.page() > 1 ) {
                            params.page(params.page() -1 );
                            pageData = filteredData.slice((params.page()-1) * params.count(),params.page() * params.count());
                        }
                        $defer.resolve(pageData);
                    }
                }
            }
        );

        $scope.saveMissingProperties = function(propslist, dialog_fct, onsuccess_fct) {
            var correctedMissingProperties = [];
            $.each(propslist, function() {
                if (this.value && this.version) {
                    correctedMissingProperties.push(this);
                }
            });
            console.log("Properties to save ");
            console.log(correctedMissingProperties);
            dialog_fct(correctedMissingProperties,function OnOK() {
                $scope.saveProperties(correctedMissingProperties,onsuccess_fct);
            }, function OnKO(){
                console.log("You Refused !!! ");
            });
        };

        $scope.noDialogOK = function(properties, OnOK_fct, OnKO_fct) {
            OnOK_fct();
        };
        $scope.noDialogKO = function(properties, OnOK_fct, OnKO_fct) {
            OnKO_fct();
        };

        // If a value is added or removed from the array
        $scope.$watch("missingprops.length", function () {
            $scope.checkpropsParams.reload();
        });
        //////////////////////////////////////////
        // Recherche
        //////////////////////////////////////////

        $scope.showProperties = function() {
            $scope.serverresponse = {};
            var properties = dgserver.listprops({platform : $scope.selectedPf, version: $scope.version},function() {
                    $scope.serverresponse.servererror=null;
                    return properties;
                }, function(errOut) {
                    console.log(errOut);
                    $scope.serverresponse.servererror=errOut;
                });
            return properties;
        };

        $scope.editedProperties = {};
        $scope.activateEdition = function(prop) {
            $scope.editedProperties[prop.id] = $.extend(true, {}, prop);
        };

        $scope.updateProperty = function(prop) {
            var response = uimanager.updateProperty(prop,$scope.editedProperties,function() {
                    $scope.servererror=null;
                    $scope.editedProperties[prop.id] = null;
                    if (prop.updatevalueforversion) {
                        console.log("reloading table");
                        console.log(prop);
                        $scope.searchedProperties=$scope.showProperties();
                        $scope.searchpropsParams.reload();
                    }
                });
            $scope.serverresponse = response;
        };

        $scope.cancelUpdate = function(prop) {
            for(var k in $scope.editedProperties[prop.id])
                prop[k]=$scope.editedProperties[prop.id][k];
            $scope.editedProperties[prop.id] = null;

        };

        $scope.searchpropsParams = new ngTableParams({
                page: 1, // show first page
                count: 100
            }, {
                total: function () {
                    var total = $scope.searchedProperties ? $scope.searchedProperties.length : 0;
                    return total;
                },
                getData: function ($defer, params) {
                    if ($scope.searchedProperties) {
                        // When the user deletes the search text, this object keeps a variable with an empty string. The
                        // strict comparator will accept only empty strings. To overcome this problem we delete the property
                        for (var propt in params.filter()) {
                            if (params.filter()[propt] === '') {
                              delete params.filter()[propt];
                            }
                        }
                        var filteredData = params.filter() ? $filter('filter')($scope.searchedProperties, params.filter()) : $scope.searchedProperties;
                        params.total(filteredData.length);
                        var pageData = filteredData.slice((params.page()-1) * params.count(),params.page() * params.count());
                        if (pageData.length == 0 && params.page() > 1 ) {
                            params.page(params.page() -1 );
                            pageData = filteredData.slice((params.page()-1) * params.count(),params.page() * params.count());
                        }
                        $defer.resolve(pageData);
                    }
                }
            }
        );

        // If a value is added or removed from the array
        $scope.$watch("searchedProperties.length", function () {
            $scope.searchpropsParams.reload();
        });


        $scope.getTemplateOrValue = function(prop) {
            if (prop.template) {
                prop.value = '# Included in \'' + prop.template + '\' #';
            } else if (prop.template == '') {
                prop.template = null;
                prop.value = null;
            }
        };

        $scope.checkmissingproperties = function(selectedPf,version) {
            console.log("PF = " + selectedPf + "| Version = " + version);

            var modalInstance = $modal.open({
                templateUrl: 'testLogsModalContent.html',
                controller: 'CheckPropertiesLogsModalCtrl',
                size: 'lg',
                resolve: {
                    serviceurl: function () {
                        return "/cgi-bin/check-properties-vs-packages-web.sh?platform="+selectedPf+"&version="+version;
                    }
                }
            });
        };

    }
]);
ipcControllers.controller('CheckPropertiesLogsModalCtrl', function ($scope, $modalInstance, propertieschecker,serviceurl) {

    $scope.scriptlog = [];
    // handles the callback from the received event
    var handleCallback = function (msg) {
        console.log(msg);
        $scope.$apply(function () {
            var data = JSON.parse(msg.data);
            if (data.missing) {
                $scope.missingproperties = data.missing;
            } else {
                $scope.scriptlog.push(data);
            }
        });
    }

    var source = new EventSource(serviceurl);
    source.onerror = function()  {
        console.log("Fin de la connexion.");
        source.close();
        console.log($scope.scriptlog);
    };
    source.addEventListener('message', handleCallback, false);

    $scope.getLogLevelStyle = function(level) {
        var stylemap = {};
        stylemap['INFO'] = 'bg-info';
        stylemap['WARN'] = 'bg-warning';
        stylemap['ERROR'] = 'bg-danger';
        return stylemap[level];
    };

});


ipcControllers.controller('listPropertiesCtrl', ['$scope', '$filter', '$modal', '$http', 'ngTableParams', 'dgserver','dialogs', 'fileservice',
    function ($scope, $filter, $modal, $http, ngTableParams, dgserver, dialogs, fileservice)
      {

        $scope.initEnumsList = function() {
            dgserver.listdomains({},function(injectedData){
                $scope.domainList = injectedData;
            }, function(errOut) {
                console.log(errOut);
                $scope.serverresponse.succeeded = false;
                $scope.serverresponse.servererror = errOut;
            });
            dgserver.listproptypes({},function(injectedData){
                $scope.valuetypeList = injectedData;
            }, function(errOut) {
                console.log(errOut);
                $scope.serverresponse.succeeded = false;
                $scope.serverresponse.servererror = errOut;
            });
        }

        $scope.editedProperties = {};
        $scope.activateEdition = function(prop) {
            $scope.editionActivated = true;
            $scope.editedProperties[prop.name] = $.extend(true, {}, prop);
        };


        $scope.updateProperty = function(prop) {
            if ($scope.removalActivated)  {
                dgserver.removePropertyinfo({propname: prop.name},function() {
                    $scope.propsinfolist = $.grep($scope.propsinfolist, function(e){
                         return e.name != prop.name;
                    });
                    $scope.editedProperties[prop.name] = null;
                    $scope.serverresponse.succeeded = true;
                    $scope.serverresponse.servererror = null;
                    $scope.removalActivated = false;
                    $scope.editionActivated = false;
                }, function(errOut) {
                    console.log(errOut);
                    $scope.serverresponse.succeeded = false;
                    $scope.serverresponse.servererror = errOut;
                });
            } else {
                console.log("Updating property with name " + prop.name);
                dgserver.modifyPropertyinfo({},prop,function() {
                    $scope.editedProperties[prop.name] = null;
                    $scope.serverresponse.succeeded = true;
                    $scope.serverresponse.servererror = null;
                    $scope.removalActivated = false;
                    $scope.editionActivated = false;
                }, function(errOut) {
                    console.log(errOut);
                    $scope.serverresponse.succeeded = false;
                    $scope.serverresponse.servererror = errOut;
                });
            }

        };

        $scope.cancelUpdate = function(prop) {
            for(var k in $scope.editedProperties[prop.name])
                prop[k]=$scope.editedProperties[prop.name][k];
            $scope.editedProperties[prop.name] = null;
            $scope.removalActivated = false;
            $scope.editionActivated = false;
        };

        $scope.activateRemoval = function(prop) {
            $scope.removalActivated = true;
            $scope.editedProperties[prop.name] = $.extend(true, {}, prop);
        };

        $scope.propsinfolist = [];

        $scope.listPropertyinfo = function() {
            $scope.serverresponse = {};
            var properties = dgserver.listpropertiesinfo({},function() {
                    $scope.serverresponse.servererror=null;
                    $scope.propsinfolist = properties;
                }, function(errOut) {
                    console.log(errOut);
                    $scope.serverresponse.servererror=errOut;
                });
        }

        $scope.propslistParams = new ngTableParams({
                page: 1, // show first page
                count: 100
            }, {
                total: function () {
                    var total = $scope.propsinfolist ? $scope.propsinfolist.length : 0;
                    return total;
                },
                getData: function ($defer, params) {
                    if ($scope.propsinfolist) {
                        // When the user deletes the search text, this object keeps a variable with an empty string. The
                        // strict comparator will accept only empty strings. To overcome this problem we delete the property
                        for (var propt in params.filter()) {
                            if (params.filter()[propt] == '') {
                              delete params.filter()[propt];
                            }
                        }
                        var filteredData = params.filter() ? $filter('filter')($scope.propsinfolist, params.filter()) : $scope.propsinfolist;
                        params.total(filteredData.length);
                        var pageData = filteredData.slice((params.page()-1) * params.count(),params.page() * params.count());
                        if (pageData.length == 0 && params.page() > 1 ) {
                            params.page(params.page() -1 );
                            pageData = filteredData.slice((params.page()-1) * params.count(),params.page() * params.count());
                        }
                        $defer.resolve(pageData);
                    }
                }
            }
        );
        // http://stackoverflow.com/questions/22892908/ng-table-typeerror-cannot-set-property-data-of-null
        $scope.propslistParams.settings().$scope = $scope;

        // If a value is added or removed from the array
        $scope.$watch("propsinfolist.length", function () {
            $scope.propslistParams.reload();
        });

        $scope.showPropInfo = function(prop) {
            $scope.serverresponse = {};
            var propinfo = dgserver.propinfo({name:prop.name}, function() {
                $scope.serverresponse.servererror=null;
                $scope.selectedPropInfo = { name: prop.name,
                                           description: prop.description,
                                           valuetype: prop.valuetype,
                                           domain: prop.domain,
                                           valuesList: propinfo};
                console.log($scope.selectedPropInfo);
                var modalInstance = $modal.open({
                    templateUrl: 'pages/property-info.html',
                    controller: 'InfosModalCtrl',
                    size: 'lg',
                    resolve: {
                        infos: function () {
                            return $scope.selectedPropInfo;
                        }
                    }
                });
            }, function(errOut) {
                console.log(errOut);
                $scope.serverresponse.servererror=errOut
            });

        };
    }
]);

ipcControllers.controller('InfosModalCtrl', function ($scope, $filter, $modalInstance, ngTableParams, dgserver, uimanager, infos) {
    console.log(infos);
    $scope.infos=infos;

    $scope.editedProperties = {};
    $scope.modificationActivated = false;

    $scope.activateEdition = function(prop) {
        if (!$scope.modificationActivated) {
            $scope.editedProperties[prop.id] = $.extend(true, {}, prop);
            $scope.modificationActivated = true;
        }
    };

    $scope.activateRemoval = function(prop) {
        if (!$scope.modificationActivated) {
            $scope.editedProperties[prop.id] = $.extend(true, {}, prop);
            $scope.modificationActivated = true;
            $scope.removalActivated = true;
        }
    };

    $scope.updateProperty = function(prop) {
        if ($scope.removalActivated)  {
            var response = uimanager.removeProperty(prop,$scope.editedProperties, function () {
                $scope.infos.valuesList = $.grep($scope.infos.valuesList, function(e){
                     return e.id != prop.id;
                });
                $scope.modificationActivated = false;
                $scope.removalActivated = false;
                $scope.infosParams.reload();
                $scope.serverresponse = null;
            });
            $scope.serverresponse = response;
        } else if($scope.additionActivated) {
            console.log("adding new property value with temp id " + prop.id);
            console.log(prop);
            if (prop.environment == 'all') {
                prop.environment = null;
            }
            if (prop.platform == 'all') {
                prop.platform = null;
            }
            if (prop.country == 'all') {
                prop.country = null;
            }
            dgserver.addprops({},[prop],function() {
                    $scope.serverresponse = null;
                    $scope.modificationActivated = false;
                    $scope.removalActivated = false;
                    $scope.additionActivated=false;
                    $scope.editedProperties[prop.id] = null;
                    dgserver.propinfo({name:$scope.infos.name}, function(propinfo) {
                            $scope.serverresponse = null;
                            $scope.infos.valuesList = propinfo;
                            $scope.infosParams.reload();
                        }, function(errOut) {
                            console.log(errOut);
                            $scope.serverresponse = {servererror: errOut} ;
                        });
                },function(errOut){
                    console.log(errOut);
                    $scope.serverresponse = {servererror: errOut} ;
                });

        } else {
            console.log("Updating property with id " + prop.id);
            var response = uimanager.updateProperty(prop,$scope.editedProperties, function () {
                $scope.modificationActivated = false;
                $scope.removalActivated = false;
                $scope.serverresponse = null;
            });
            $scope.serverresponse = response;
        }

    };

    $scope.cancelUpdate = function(prop) {
        if ($scope.additionActivated) {
            $scope.infos.valuesList = $.grep($scope.infos.valuesList, function(e){
                 return e.id != prop.id;
            });
        }
        for(var k in $scope.editedProperties[prop.id])
            prop[k]=$scope.editedProperties[prop.id][k];
        $scope.editedProperties[prop.id] = null;
        $scope.modificationActivated = false;
        $scope.removalActivated = false;
        $scope.serverresponse = null;
        $scope.additionActivated = false;
    };

    $scope.close = function () {
        $modalInstance.close();
    };

    var getMaxId = function(valuesList) {
      return Math.max.apply(Math,valuesList.map(function(o){return o.id;}))
    };

    $scope.addNewProperty = function() {
        if (!$scope.modificationActivated && !$scope.removalActivated) {
            var newProperty = {
                    id: getMaxId($scope.infos.valuesList) * -1,
                    name: $scope.infos.name,
                    value: undefined,
                    environment: undefined,
                    platform: undefined,
                    country: undefined,
                    version: undefined,
                    version_limit: undefined,
                    template: undefined
                };
            $scope.editedProperties[newProperty.id] = $.extend(true, {}, newProperty);
            $scope.modificationActivated = true;
            $scope.additionActivated = true;
            $scope.infos.valuesList.push(newProperty);
        }
    };

    $scope.infosParams = new ngTableParams({
            page: 1, // show first page
            count: 10,
            $scope: { $data: {} }
        }, {
                total: function () {
                    var total = $scope.infos.valuesList ? $scope.infos.valuesList.length : 0;
                    return total;
                },
                getData: function ($defer, params) {
                    if ($scope.infos.valuesList) {
                        // When the user deletes the search text, this object keeps a variable with an empty string. The
                        // strict comparator will accept only empty strings. To overcome this problem we delete the property
                        for (var propt in params.filter()) {
                            if (params.filter()[propt] === '') {
                              delete params.filter()[propt];
                            }
                        }
                        var filteredData = params.filter() ? $filter('filter')($scope.infos.valuesList, params.filter()) : $scope.infos.valuesList;
                        params.total(filteredData.length);
                        var pageData = filteredData.slice((params.page()-1) * params.count(),params.page() * params.count());
                        if (pageData.length == 0 && params.page() > 1 ) {
                            params.page(params.page() -1 );
                            pageData = filteredData.slice((params.page()-1) * params.count(),params.page() * params.count());
                        }
                        $defer.resolve(pageData);
                    }
                },
                $scope: { $data: {} }
            });

    // http://stackoverflow.com/questions/22892908/ng-table-typeerror-cannot-set-property-data-of-null
    $scope.infosParams.settings().$scope = $scope;

    // If a value is added or removed from the array
    $scope.$watch("infos.valuesList.length", function () {
        $scope.infosParams.reload();
    });

    $scope.initPFList = function() {
        dgserver.listpf({}, function(injectedPL) {
            $scope.envmanager = uimanager.getEnvManager(injectedPL);
            $scope.envmanager.getTemplates();
            $scope.envmanager.listversions();
        });
        $scope.uimgr = uimanager;

    };


});

ipcControllers.controller('ReusedPropertiesCtrl', function ($scope, ngTableParams, proplist){
  console.log(proplist);
  $scope.proplist=proplist;

});

ipcControllers.controller('confirmDialogCtrl', function ($scope, $modalInstance, ngTableParams, data) {
        console.log(data);
        $scope.infos=data;

        $scope.cancel = function(){
            $modalInstance.dismiss('canceled');
        }; // end cancel

        $scope.save = function(){
            $modalInstance.close();
        }; // end save

        $scope.propslistParams = new ngTableParams({
                page: 1, // show first page
                count: 10
            }, {
                total: function () {
                    var total = $scope.infos.properties ? $scope.infos.properties : 0;
                    return total;
                },
                getData: function ($defer, params) {
                    if ($scope.infos.properties) {
                        // When the user deletes the search text, this object keeps a variable with an empty string. The
                        // strict comparator will accept only empty strings. To overcome this problem we delete the property
                        for (var propt in params.filter()) {
                            if (params.filter()[propt] === '') {
                              delete params.filter()[propt];
                            }
                        }
                        params.total($scope.infos.properties.length);
                        var pageData = $scope.infos.properties.slice((params.page()-1) * params.count(),params.page() * params.count());
                        if (pageData.length == 0 && params.page() > 1 ) {
                            params.page(params.page() -1 );
                            pageData = $scope.infos.properties.slice((params.page()-1) * params.count(),params.page() * params.count());
                        }
                        $defer.resolve(pageData);
                    }
                }
            });
    }
).config(['dialogsProvider','$translateProvider',function(dialogsProvider,$translateProvider){
		dialogsProvider.useBackdrop('static');
		dialogsProvider.useEscClose(false);
		dialogsProvider.useCopy(false);
		dialogsProvider.setSize('lg');
}]);
