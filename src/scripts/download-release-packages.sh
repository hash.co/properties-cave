#!/bin/bash

DL_FOLDER_BASE="/opt/application/"
NEXUS_URL="https://nexus.strator.eu/nexus/service/local/artifact/maven/content"
IPC_URL="https://gdp.strator.eu/dwarfgarden/ipc.php?action=getPlatformPropertiesFile"

mdp=CNbitsU

# log a message
# log <message>
log(){
	#echo -ne "${FUNCNAME[2]}(${BASH_LINENO[1]})\t:${me}dd${r} $(date +%x-%X)" >&2
	#printf "%-13s [%-16s] %-30s " "${r}${me}" "$(date +%x-%X)" "${FUNCNAME[2]}(${BASH_LINENO[1]})" >&2
	printf "%-12s (%s) %24s " "$(hostname -s)" "$(date +%H:%M:%S)" "${FUNCNAME[2]}(${BASH_LINENO[1]})" >&2
	echo -e "$@" >&2
}

log_info() {
_bg="\033[42m"
_re="\033[m"
	log "$_bg INFO  $_re" "$*"
}

log_error() {
_br="\033[41m"
_re="\033[m"
	log "$_br ERROR $_re" "$*"
}

log_warn() {
_by="\033[43m"
_re="\033[m"
	log "$_by WARN  $_re" "$*"
}


md5() { #Return the md5 sum a $1, wether it is a file or a string
        if [ -e "$1" ]
        then
                cat "$1" | openssl md5 | sed -r -e 's/([^=]*= *)?(.*)/\2/'
        else
                echo -n "$1" | openssl md5 | sed -r -e 's/([^=]*= *)?(.*)/\2/'
        fi
}

# rest call : groupeId:artifactId:type:version:classifier outputfile
download_via_nexus() {
	# Get user password option
	local passwd_opts="--no-check-certificate --http-user=developer --http-password=$mdp"

	# Get REST download option
	local rest_download=$(echo $1 | { IFS=: read  groupeid artifactid type version classifier ; echo "g=$groupeid&a=$artifactid&e=$type&v=$version&c=$classifier"  ; })
	local rest_download_md5=$(echo $1 | { IFS=: read groupeid artifactid type version classifier ; echo "g=$groupeid&a=$artifactid&e=$type.md5&v=$version&c=$classifier"  ; })

	# Choose the repository
	local nexus_repo=releases
	local snapshot_found=$(echo $1 | cut -d : -f 4 | grep SNAPSHOT)
	if [ $snapshot_found ]; then
		nexus_repo=snapshots
	fi
	#added by rani
	local thirdpart_found=$(echo "$1" | grep zapcat)
	if [ "$thirdpart_found" ]; then
		nexus_repo="thirdparty"
	fi
	# Getting the md5 sum of the artifact. If I can't find it, things will probably turn bad
	local classifier=$(echo $1 | cut -d ':' -f 5)
	local artifactid=$(echo $1 | cut -d ':' -f 2)
	local version=$(echo $1 | cut -d ':' -f 4)
	# Trick to deal with set -e
	# As set -e exits if wget exit with a non-zero code, let say that the $() always returns with 0, but send  md5 sum and exit code on standard output
	#wget -nc -nv $passwd_opts "$NEXUS_URL?r=$nexus_repo&$rest_download_md5" -O - 2>/dev/null ; echo -e "\n$?"
	local r
	read md5_sum r <<< "$(wget -nc -nv $passwd_opts "$NEXUS_URL?r=$nexus_repo&$rest_download_md5" -O - 2>/dev/null ; echo " $?")"

	# if r is empty, it's that wget return empty strings, thus 'read' put exit code in md5_sum
	[ -z "$r" ] && r=$md5_sum
	case $r in
		8|1)
		log_error "'Error 404' while trying to download the md5 sum of '$artifactid-$classifier' v$version. Inexistant version ?" ;;
		6)
		log_error "'Error 401' while trying to download the md5 sum of $artifactid-$classifier v$version. Wrong credential ?" ;;
		0)
		;;
		*)
		log_error "Unknown wget's error code : $r" ;;
	esac
	[ $r -ne 0 ] && exit 1

	# Choose Output filename
	local output_opts="--content-disposition"
	if [ $2 ]; then
		output_opts="-O '$2'"
		if [ -e "$2" ]
		then
			# If I have the md5 sum and the destination file exists and both md5 match, no need to dl again
			if [ "$(md5 "$2")" = "$md5_sum" ]
			then
				log_info "$artifactid-$classifier v$version already downloaded ;)"
				local already_downloaded=1
			else
				log_warn "$2 exists but checksum differs"
			fi
		fi
	fi
	if [ ! $already_downloaded ]
	then
		local wget_cmd="wget -nc -nv $passwd_opts '$NEXUS_URL?r=$nexus_repo&$rest_download' $output_opts 2>/dev/null"
		log_info "Launching: $wget_cmd"
		rm -f "$2"
		eval $wget_cmd || r=$?
	fi

	if [ $r -ne 0 -a ! -e $2 ]
	then
		# Clean the file if created
		if [ $2 ] && [ -f $2 ]; then
			rm -f $2
		fi
		log_error "File not downloaded. Wrong password ? Inexistant version ? (you asked $version)"
		log_error "Please look at wget's errors"
		exit 1
	fi
}

_help() {
	cat <<EOF
	USAGE $0 <platform> <version> [destination_folder]

        platform : the platform to install (one of PROD,DEMO,QUA1,QUA2,UAT1,UAT2,UAT3)
		version : the version to deploy
		destination_folder : OPTIONAL destination to put the files in. If not set, the folder base will be "$DL_FOLDER_BASE"
EOF
}

setupPlatformVars() {
    local pf=$1
    platform="$pf"
    case $pf in
        PROD|ES_PROD1)
            platform="ES_PROD1"
            vente_env=prod
            [ -z "$dest_folder" ] && dest_folder="$DL_FOLDER_BASE/$pf/deploy/$pf-$version/"
            ;;
        ES_PROD*)
            vente_env=prod
            [ -z "$dest_folder" ] && dest_folder="$DL_FOLDER_BASE/$pf/deploy/$pf-$version/"
            ;;
        DEMO|ES_DEMO1)
            platform="ES_DEMO1"
            vente_env=prod
            [ -z "$dest_folder" ] && dest_folder="$DL_FOLDER_BASE/DEMO/deploy/DEMO-$version/"
            ;;
        ES_QUA*)
            vente_env=qa
            [ -z "$dest_folder" ] && dest_folder="$DL_FOLDER_BASE/$pf/deploy/$pf-$version/"
            ;;
        UAT?)
            vente_env=qa
            [ -z "$dest_folder" ] && dest_folder="$DL_FOLDER_BASE/$pf/deploy/$pf-$version/"
            ;;
        *)
            [ -z "$dest_folder" ] && dest_folder="$DL_FOLDER_BASE/$pf/deploy/$pf-$version/"
            [[ "$pf" == DEV? ]] && vente_env=dev && return;
            [[ "$pf" == REC? ]] && vente_env=qa && return;
            [[ "$pf" == QUA? ]] && vente_env=qa && return;
            [[ "$pf" == PROD? ]] && vente_env=prod && return;
            [[ "$pf" == DEMO? ]] && vente_env=qa && return;
            [[ "$pf" == AUTOMATE ]] && vente_env=qa && return;
            [[ "$pf" == SALON ]] && vente_env=qa && return;

            log_error "this platform ($pf) is not supported"
            _help
            exit 1
            ;;
    esac
}


[ $# -eq 0 -o $# -eq 1 ] && _help && exit 1

humanPf=$1
version=$2
dest_folder=$3

setupPlatformVars $humanPf


mkdir -p "$dest_folder/vente"

#download properties file
curl --silent -k --user "dev:CNbitsUHard" "$IPC_URL&platform=$platform&version=$version" > $dest_folder/TPOS-${humanPf}.conf

#download_via_nexus "com.strator.iris:winmobile:cab:$version:installer" "$dest_folder/tpi-$version.cab"

download_via_nexus "com.strator.iris.server.configuration:gestion-deploy:zip:$version:config" "$dest_folder/gestion-$version.zip"
download_via_nexus "com.strator.iris.server.configuration:gouroo-deploy:zip:$version:config" "$dest_folder/gouroo-$version.zip"
download_via_nexus "com.strator.iris.server.configuration:middleoffice-deploy:zip:$version:config" "$dest_folder/mo-$version.zip"
download_via_nexus "com.strator.iris.server.configuration:database-deploy:zip:$version:config" "$dest_folder/irisdb-$version.zip"
download_via_nexus "com.strator.iris.server.configuration:gouroo-database-deploy:zip:$version:config" "$dest_folder/gouroodb-$version.zip"
download_via_nexus "com.strator.iris.server.configuration:reporting-deploy:zip:$version:config" "$dest_folder/iris_reportingdb-$version.zip"

download_via_nexus "com.strator.iris:vente-encaissement:zip:$version:vente-env-$vente_env" "$dest_folder/vente/vente-env-$vente_env-$version.zip"
download_via_nexus "com.strator.iris:vente-encaissement:zip:$version:vente-installer" "$dest_folder/vente/vente-installer-$version.zip"
download_via_nexus "com.strator.iris:vente-encaissement:zip:$version:vente-common" "$dest_folder/vente/vente-common-$version.zip"

dest="$dest_folder/vente-$version"
mkdir -p "$dest"

unzip -o -q -d "$dest" "$dest_folder/vente/vente-installer-$version.zip" ; r=$?
[ "$r" != "0" ] && log_error "problem unzipping $dest_folder/vente/vente-installer-$version.zip" && exit 1
unzip -o -q -d "$dest/VentePSInstaller/Vente" "$dest_folder/vente/vente-common-$version.zip" ; r=$?
[ "$r" != "0" ] && log_error "problem unzipping $dest_folder/vente/vente-common-$version.zip" && exit 1
unzip -o -q -d "$dest/VentePSInstaller/Vente" "$dest_folder/vente/vente-env-$vente_env-$version.zip" ; r=$?
[ "$r" != "0" ] && log_error "problem unzipping $dest_folder/vente/vente-env-$vente_env-$version.zip" && exit 1

cd "$dest_folder"

if [ ! -r "vente-$version.zip" -o "$(cat "vente-$version.zip.md5" 2>/dev/null)" != "$(md5 "vente-$version.zip")"  ]
then
    zip -qr "vente-$version.zip" "vente-$version"
    md5 vente-$version.zip > vente-$version.zip.md5
fi

cd -

rm -rf "$dest"
