--
DO $$
  DECLARE ret BOOLEAN;
BEGIN
  ret := (select version_between('1.20','1.00','2.00'));
  PERFORM assert_true(ret);

  ret := (select version_between('0','1.00','2.00'));
  PERFORM assert_false(ret);
END $$;
