--
--
--
--
-- Definition for function assert_true
--
CREATE FUNCTION assert_true (flag boolean) RETURNS void AS
$body$
BEGIN
    IF flag IS NOT TRUE THEN
        RAISE EXCEPTION '%', E'Expected to be true\n';
    END IF;
END
$body$
    LANGUAGE plpgsql;
--
-- Definition for function assert_false
--
CREATE FUNCTION assert_false (flag boolean) RETURNS void AS
$body$
BEGIN
    IF flag IS NOT FALSE THEN
        RAISE EXCEPTION '%', E'Expected to be true\n';
    END IF;
END
$body$
    LANGUAGE plpgsql;
--
-- Definition for function assert_same
--
CREATE FUNCTION assert_same (in_expected character varying, in_got character varying) RETURNS void AS
$body$
BEGIN
    IF in_expected IS DISTINCT FROM in_got THEN
        RAISE EXCEPTION ' get a different value from the expected one %', E'\n      expected : [' || in_expected || E']\n      got      : [' || in_got || E']\n';
    END IF;
END
$body$
    LANGUAGE plpgsql;
--
