#!/bin/bash

# Change owner of /var/log/ipc 

chown -R www-data:www-data /var/log/ipc

# Setup the database

/opt/setupdb/setup.sh

# launch standard cmd

apache2-foreground


