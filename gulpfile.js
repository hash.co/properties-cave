var gulp = require('gulp');
var useref = require('gulp-useref');
var uglify = require('gulp-uglify');
var gulpIf = require('gulp-if');
var cssnano = require('gulp-cssnano');
var imagemin = require('gulp-imagemin');
var del = require('del');
var cache = require('gulp-cache');
var runSequence = require('run-sequence');
var bower = require('gulp-bower');
var rev = require('gulp-rev');
var revAppend = require('gulp-rev-append');
var revReplace = require('gulp-rev-replace');
var exec = require('child_process').exec;
var merge = require('merge-stream');

gulp.task('default', function (callback) {
    runSequence('clean', ['app', 'db'],'docker-build', callback)
});

gulp.task('clean', function () {
        return del.sync('target');
});

gulp.task('app', function () {
    return gulp.src(['src/web/**/*'])
            .pipe(gulp.dest('target/properties-cave/web'))
});

gulp.task('db', function () {
    return gulp.src(['src/database/**/*'])
            .pipe(gulp.dest('target/properties-cave/database'))
});

gulp.task('dockerfiles', function () {
    return gulp.src(['docker/**/*'])
            .pipe(gulp.dest('target/properties-cave'))
});

gulp.task('docker-build', ['app', 'db', 'dockerfiles'],  function (callback) {
    exec('docker build target/properties-cave --tag properties-cave', function (err, stdout, stderr) {
        console.log(stdout);
        console.log(stderr);
        callback(err);
    });
});

gulp.task('docker-stop',  function (callback) {
    exec('docker-compose down', function (err, stdout, stderr) {
        console.log(stdout);
        console.log(stderr);
        callback(err);
    });
});

gulp.task('devmode', ['docker-stop', 'docker-build'],  function (callback) {
    exec('docker-compose up -d', function (err, stdout, stderr) {
        console.log(stdout);
        console.log(stderr);
        callback(err);
    });
});

